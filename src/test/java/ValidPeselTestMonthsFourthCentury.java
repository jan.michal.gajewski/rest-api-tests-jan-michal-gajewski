import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class ValidPeselTestMonthsFourthCentury {

    @Test
    public void fourthCenturyJanuaryMonthCode41(){
        String Pesel = "05410558303";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2105-01-05T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyFebruaryMonthCode42(){
        String Pesel = "13420753704";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2113-02-07T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyMarchMonthCode43(){
        String Pesel = "21430931324";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2121-03-09T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyAprilMonthCode44(){
        String Pesel = "29441189853";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2129-04-11T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyMayMonthCode45(){
        String Pesel = "37451300379";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2137-05-13T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyJuneMonthCode46(){
        String Pesel = "45461554576";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2145-06-15T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyJulyMonthCode47(){
        String Pesel = "53471707178";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2153-07-17T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyAugustMonthCode48(){
        String Pesel = "61481948614";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2161-08-19T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturySeptemberMonthCode49(){
        String Pesel = "69492160810";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2169-09-21T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyOctoberMonthCode50(){
        String Pesel = "77502314508";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2177-10-23T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyNovemberMonthCode51(){
        String Pesel = "85512567629";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2185-11-25T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fourthCenturyDecemberMonthCode52(){
        String Pesel = "93522705544";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2193-12-27T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

}
