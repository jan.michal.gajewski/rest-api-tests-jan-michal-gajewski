import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class InvalidPeselTestInvalidLength {

    @Test
    public void invalidPeselLength_10MarksLength() {
        String Pesel = "1234567891";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = null;
        String expectedGender = null;
        String expectedErrorCode1 = "INVL";
        String expectedErrorMessage1 = "Invalid length. Pesel should have exactly 11 digits.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode1 = response.path("errors[0].errorCode");
        String responseErrorMessage1 = response.path("errors[0].errorMessage");

        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode1, expectedErrorCode1);
        Assert.assertEquals(responseErrorMessage1, expectedErrorMessage1);
    }

    @Test
    public void invalidPeselLength_12MarksLength() {
        String Pesel = "258369456852";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = null;
        String expectedGender = null;
        String expectedErrorCode = "INVL";
        String expectedErrorMessage = "Invalid length. Pesel should have exactly 11 digits.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void invalidPeselLength_EmptyPeselField() {
        String Pesel = "";
        String expectedType = "https://tools.ietf.org/html/rfc7231#section-6.5.1";
        String expectedTitle = "Bad Request";
        int expectedStatus = 400;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responseType = response.path("type");
        String responseTitle = response.path("title");
        int responseStatus = response.path("status");

        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseTitle, expectedTitle);
        Assert.assertEquals(responseType, expectedType);
    }
}
