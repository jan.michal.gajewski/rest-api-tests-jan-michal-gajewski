import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class ValidPeselTestMonthsFifthCentury {

    @Test
    public void fifthCenturyJanuaryMonthCode61(){
        String Pesel = "04610699081";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2204-01-06T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyFebruaryMonthCode62(){
        String Pesel = "12620873836";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2212-02-08T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyMarchMonthCode63(){
        String Pesel = "20631083067";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2220-03-10T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyAprilMonthCode64(){
        String Pesel = "28641221693";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2228-04-12T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyMayMonthCode65(){
        String Pesel = "36651470185";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2236-05-14T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyJuneMonthCode66(){
        String Pesel = "44661651012";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2244-06-16T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyJulyMonthCode67(){
        String Pesel = "52671851845";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2252-07-18T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyAugustMonthCode68(){
        String Pesel = "60682087276";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2260-08-20T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturySeptemberMonthCode69(){
        String Pesel = "68692243462";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2268-09-22T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyOctoberMonthCode70(){
        String Pesel = "76702474779";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2276-10-24T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyNovemberMonthCode71(){
        String Pesel = "84712665708";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2284-11-26T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void fifthCenturyDecemberMonthCode72(){
        String Pesel = "92722878070";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2292-12-28T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

}
