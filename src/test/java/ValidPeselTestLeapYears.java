import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class ValidPeselTestLeapYears {   // Done Here

    @Test
    public void validPesel_LeapYearFirstCentury() {
        String Pesel = "68822982362";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1868-02-29T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void validPesel_LeapYearSecondCentury() {
        String Pesel = "52022998366";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1952-02-29T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void validPesel_LeapYearThirdCentury() {
        String Pesel = "16222939503";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2016-02-29T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void validPesel_LeapYearFourthCentury() {
        String Pesel = "16422920781";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2116-02-29T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void validPesel_LeapYearFifthCentury() {
        String Pesel = "12622999987";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "2212-02-29T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }




























}
