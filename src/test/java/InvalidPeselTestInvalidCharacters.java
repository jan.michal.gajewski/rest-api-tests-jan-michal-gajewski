import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class InvalidPeselTestInvalidCharacters {

    @Test
    public void invalidPeselCharacter_ExtraCapitalLetter() {
        String Pesel = "920322A4437";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = null;
        String expectedGender = null;
        String expectedErrorCode = "NBRQ";
        String expectedErrorMessage = "Invalid characters. Pesel should be a number.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    public void invalidPeselCharacter_ExtralowercaseLetter() {
        String Pesel = "920322b4437";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = null;
        String expectedGender = null;
        String expectedErrorCode = "NBRQ";
        String expectedErrorMessage = "Invalid characters. Pesel should be a number.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void invalidPeselCharacter_ExtraAlphanumericCharacter() {
        String Pesel = "920#2226156";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = null;
        String expectedGender = null;
        String expectedErrorCode = "NBRQ";
        String expectedErrorMessage = "Invalid characters. Pesel should be a number.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }
}
