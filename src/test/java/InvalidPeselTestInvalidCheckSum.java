import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class InvalidPeselTestInvalidCheckSum {

    @Test
    public void invalidCheckSum() {
        String Pesel = "92722878071";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = "2292-12-28T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = "INVC";
        String expectedErrorMessage = "Check sum is invalid. Check last digit.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }
}
