import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class InvalidPeselTestInvalidMonthAndYear {

    @Test
    public void invalidPeselMonth_InvalidMonthAndYear() {
        String Pesel = "00000000000";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = null;
        String expectedGender = "Female";
        String expectedErrorCode1 = "INVY";
        String expectedErrorMessage1 = "Invalid year.";
        String expectedErrorCode2 = "INVM";
        String expectedErrorMessage2 = "Invalid month.";
        String expectedErrorCode3 = "INVD";
        String expectedErrorMessage3 = "Invalid day.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode1 = response.path("errors[0].errorCode");
        String responseErrorMessage1 = response.path("errors[0].errorMessage");
        String responseErrorCode2 = response.path("errors[1].errorCode");
        String responseErrorMessage2 = response.path("errors[1].errorMessage");
        String responseErrorCode3 = response.path("errors[2].errorCode");
        String responseErrorMessage3 = response.path("errors[2].errorMessage");


        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode1, expectedErrorCode1);
        Assert.assertEquals(responseErrorMessage1, expectedErrorMessage1);
        Assert.assertEquals(responseErrorCode2, expectedErrorCode2);
        Assert.assertEquals(responseErrorMessage2, expectedErrorMessage2);
        Assert.assertEquals(responseErrorCode3, expectedErrorCode3);
        Assert.assertEquals(responseErrorMessage3, expectedErrorMessage3);
    }
}
