import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;
import static io.restassured.RestAssured.get;

public class ValidPeselTestMonthsFirstCentury {

    @Test
    public void firstCenturyJanuaryMonthCode81(){
        String Pesel = "08810237414";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1808-01-02T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyFebruaryMonthCode82(){
        String Pesel = "16820442139";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1816-02-04T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyMarchMonthCode83(){
        String Pesel = "24830680511";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1824-03-06T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyAprilMonthCode84(){
        String Pesel = "32840873276";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1832-04-08T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyMayMonthCode85(){
        String Pesel = "40851096953";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1840-05-10T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyJuneMonthCode86(){
        String Pesel = "48861268353";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1848-06-12T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyJulyMonthCode87(){
        String Pesel = "57871449201";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1857-07-14T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyAugustMonthCode88(){
        String Pesel = "64881667048";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1864-08-16T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturySeptemberMonthCode89(){
        String Pesel = "72891815188";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1872-09-18T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyOctoberMonthCode90(){
        String Pesel = "80902073469";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1880-10-20T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyNovemberMonthCode91(){
        String Pesel = "88912210629";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1888-11-22T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void firstCenturyDecemberMonthCode92(){
        String Pesel = "96922404187";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1896-12-24T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

}
