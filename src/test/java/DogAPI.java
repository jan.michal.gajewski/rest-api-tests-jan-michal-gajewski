import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class DogAPI {

    @Test
    public void validDogApi() {
        boolean expectedStatus = true;

        Response response = get("https://dog-api.kinduff.com//api/facts");

        boolean responseStatus = response.path("success");
        String responsefact = response.path("facts[0]");

        Assert.assertTrue(responseStatus == expectedStatus);
        Assert.assertTrue(responsefact.length()>0);

      //  System.out.println("responseStatus: " + responseStatus);
      //  System.out.println("responsefact: " + responsefact);  // To check the funny fact :)

    }


}
