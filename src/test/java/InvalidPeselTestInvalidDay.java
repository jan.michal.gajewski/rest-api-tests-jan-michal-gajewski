import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class InvalidPeselTestInvalidDay {

    @Test
    public void invalidPeselDay_DayDoesNotExistUpperLimit() {
        String Pesel = "98013236899";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = null;
        String expectedGender = "Male";
        String expectedErrorCode = "INVD";
        String expectedErrorMessage = "Invalid day.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void invalidPeselDay_DayDoesNotExistLowerLimit() {
        String Pesel = "98010036898";
        Boolean expectedStatus = false;
        String expectedDateOfBirth = null;
        String expectedGender = "Male";
        String expectedErrorCode = "INVD";
        String expectedErrorMessage = "Invalid day.";

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        Boolean responseStatus = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatus, expectedStatus);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }
}
