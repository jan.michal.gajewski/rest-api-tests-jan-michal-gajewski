import io.restassured.response.Response;
import org.junit.Test;
import org.testng.Assert;

import static io.restassured.RestAssured.get;

public class ValidPeselTestMonthsSecondCentury {

    @Test
    public void secondCenturyJanuaryMonthCode01(){
        String Pesel = "07010301200";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1907-01-03T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyFebruaryMonthCode02(){
        String Pesel = "15020521442";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1915-02-05T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyMarchMonthCode03(){
        String Pesel = "23030730284";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1923-03-07T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyAprilMonthCode04(){
        String Pesel = "31040954604";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1931-04-09T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyMayMonthCode05(){
        String Pesel = "39051170361";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1939-05-11T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyJuneMonthCode06(){
        String Pesel = "47061316543";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1947-06-13T00:00:00";
        String expectedGender = "Female";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyJulyMonthCode07(){
        String Pesel = "55071590510";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1955-07-15T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyAugustMonthCode08(){
        String Pesel = "63081753054";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1963-08-17T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturySeptemberMonthCode09(){
        String Pesel = "71091912099";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1971-09-19T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyOctoberMonthCode10(){
        String Pesel = "79102154393";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1979-10-21T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyNovemberMonthCode11(){
        String Pesel = "87112375291";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1987-11-23T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

    @Test
    public void secondCenturyDecemberMonthCode12(){
        String Pesel = "95122503274";
        int expectedStatusCode = 200;
        Boolean expectedIsValid = true;
        String expectedDateOfBirth = "1995-12-25T00:00:00";
        String expectedGender = "Male";
        String expectedErrorCode = null;
        String expectedErrorMessage = null;

        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + Pesel);

        String responsePesel = response.path("pesel");
        int responseStatusCode = response.statusCode();
        Boolean responseIsValid = response.path("isValid");
        String responseDateOfBirth = response.path("dateOfBirth");
        String responseGender = response.path("gender");
        String responseErrorCode = response.path("errors[0].errorCode");
        String responseErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(responsePesel, Pesel);
        Assert.assertEquals(responseStatusCode, expectedStatusCode);
        Assert.assertEquals(responseIsValid, expectedIsValid);
        Assert.assertEquals(responseDateOfBirth, expectedDateOfBirth);
        Assert.assertEquals(responseGender, expectedGender);
        Assert.assertEquals(responseErrorCode, expectedErrorCode);
        Assert.assertEquals(responseErrorMessage, expectedErrorMessage);
    }

}
